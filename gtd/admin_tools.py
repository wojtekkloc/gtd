import pytz

from django.utils import timezone
from django.utils.html import format_html
from django.urls import reverse
from django.conf import settings


def format_in_tz(datetime, tz=None):
    if not tz:
        tz = pytz.timezone(settings.TIME_ZONE)
    return datetime.astimezone(tz).strftime("%Y-%m-%d %H:%M")


def as_html(field_name):
    def _as_html(obj):
        field = getattr(obj, field_name)
        if field:

            return format_html(field)
        else:
            return field

    _as_html.short_description = field_name.replace("_", " ")
    return _as_html


def linkify(field_name):
    """
    Turns foreign key field into a link.
    """

    def _linkify(obj):
        linked_obj = getattr(obj, field_name)
        if linked_obj:
            app_label = linked_obj._meta.app_label
            model_name = linked_obj._meta.model_name
            view_name = "admin:{}_{}_change".format(app_label, model_name)

            link_url = reverse(view_name, args=[linked_obj.id])
            return format_html(
                '<a href="{}" target="_blank">{}</a>', link_url, linked_obj
            )
        else:
            return linked_obj

    _linkify.short_description = field_name.replace("_", " ")
    return _linkify


def edit_id(field_name):
    """
    Turns foreign key field into a link.
    """

    def _edit_id(obj):
        linked_obj = getattr(obj, field_name)
        if linked_obj:
            app_label = linked_obj._meta.app_label
            model_name = linked_obj._meta.model_name
            view_name = "admin:{}_{}_change".format(app_label, model_name)

            link_url = reverse(view_name, args=[linked_obj.id])
            return format_html(
                '<a href="{}">EDYTUJ #{}</a>', link_url, linked_obj.id
            )
        else:
            return linked_obj

    _edit_id.short_description = field_name.replace("_", " ")
    return _edit_id


def red_if_due(field_name):
    """
    Colours date(time) field red if it is due.
    """

    def _red_if_due(obj):
        field = getattr(obj, field_name)
        is_due = timezone.now() > field if field else False
        if is_due:
            return format_html(
                '<span style="color: red">{}</span>'.format(format_in_tz(field))
            )
        else:
            return field

    _red_if_due.short_description = field_name.replace("_", " ")
    return _red_if_due


def coloured_status(field_name):
    def _coloured_status(obj):
        field = getattr(obj, field_name)
        colors = {
            "new": "red",
            "pending": "black",
            "cancelled": "darkgrey",
            "done": "green",
        }
        return format_html(
            '<span style="background-color: {}; color: white; padding: 2px 5px; border-radius: 5px;">{}</span>'.format(
                colors[field], field
            )
        )

    _coloured_status.short_description = field_name.replace("_", " ")
    return _coloured_status
