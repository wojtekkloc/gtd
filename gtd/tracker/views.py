from django.shortcuts import render
from django.views.generic import TemplateView, DetailView, CreateView

from .forms import ProjectForm
from .models import *


class Dashboard(TemplateView):
    template_name = 'gtd/dashboard.html'

    def get_context_data(self, **kwargs):
        context = super(Dashboard, self).get_context_data(**kwargs)
        context['categories'] = ProjectCategory.objects.filter(parent=None)
        context['add_project_form'] = ProjectForm()
        return context


class TaskDetails(TemplateView):
    template_name = 'gtd/_task.html'

    def get(self, request, *args, **kwargs):
        self.object = Task.objects.get(pk=kwargs['pk'])
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)


class ProjectDetails(TemplateView):
    template_name = 'gtd/_project.html'

    def get(self, request, *args, **kwargs):
        self.object = Project.objects.get(pk=kwargs['pk'])
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)
