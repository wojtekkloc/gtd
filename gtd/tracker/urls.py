from django.conf.urls import url

from .views import Dashboard, TaskDetails, ProjectDetails

app_name = 'gtd'

urlpatterns = [
    url(r'dashboard/$', Dashboard.as_view(), name='dashboard'),
    url(r'project/(?P<pk>[0-9]+)/$', ProjectDetails.as_view(), name='project-details'),
    url(r'task/(?P<pk>[0-9]+)/$', TaskDetails.as_view(), name='task-details'),

]
