import random

from django import template

register = template.Library()


quotes = [
    "Keep your tears in your eyes where they belong.",
    "Never half-ass two things. Whole-ass one thing.",
    "Any dog under fifty pounds is a cat and cats are useless.",
    "There is only one bad word: taxes.",
    "Friends: one to three is sufficient.",
    "Breakfast food can serve many purposes.",
    "Fishing is for sport only. Fish meat is practically a vegetable.",
    "Turkey can never beat cow.",
    " I was born ready. I'm Ron Fucking Swanson.",
    "My idea of a perfect government is one guy who sits in a small room at a desk, and the only thing he's allowed to decide is who to nuke. The man is chosen based on some kind of IQ test, and maybe also a physical tournament, like a decathlon. And women are brought to him, maybe...when he desires them.",
    "Capitalism: God's way of determining who is smart and who is poor.",
    "The less I know about other people’s affairs, the happier I am. I’m not interested in caring about people. I once worked with a guy for three years and never learned his name. Best friend I ever had. We still never talk sometimes.",
    "I regret nothing. The end.",
    "I like saying ‘No,’ it lowers their enthusiasm.",
    "You had me at ‘Meat Tornado.’",
    "Are you going to tell a man that he can’t fart in his own car?",
    "Put some alcohol in your mouth to block with words from coming out.",
    "Give 100%. 110% is impossible. Only idiots recommend that.",
    "When I eat, it is the food that is scared.",
    "Birthdays were invented by Hallmark to sell cards.",
    "Sting like a bee. Do not float like a butterfly. That's ridiculous.",
    "Give a man a fish and feed him for a day. Don’t teach a man to fish…and feed yourself. He’s a grown man. And fishing’s not that hard.",
    "America: The only country that matters. If you want to experience other ‘cultures,’ use an atlas or a ham radio.",
    "Fishing relaxes me. It’s like yoga, except I still get to kill something.",
    "History began on July 4, 1776. Everything that happened before that was a mistake.",
    "Just give me all the bacon and eggs you have. Wait…wait. I worry what you just heard was: Give me a lot of bacon and eggs. What I said was: Give me all the bacon and eggs you have. Do you understand?",
    "I don’t want to paint with a broad brush here, but every single contractor in the world is a miserable, incompetent thief.",
    "[On bowling] Straight down the middle. No hook, no spin, no fuss. Anything more and this becomes figure skating.",
    "The government is a greedy piglet that suckles on a taxpayer’s teat until they have sore, chapped nipples.",
    "If any of you need anything at all, too bad. Deal with your problems yourselves, like adults.",
    "When people get too chummy with me I like to call them by the wrong name to let them know I don't really care about them.",
    "I once worked with a guy for three years and never learned his name. Best friend I ever had. We still never talk sometimes.",
    "One rage every three months is permitted. Try not to hurt anyone who doesn’t deserve it.",
    "Strippers do nothing for me…but I will take a free breakfast buffet anytime, anyplace.",
    "No home is complete without a proper toolbox. Here’s April and Andy’s: A hammer, a half-eaten pretzel, a baseball card, some cartridge that says Sonic and Hedgehog, a scissor half, a flashlight filled with jellybeans.",
    "So you talked to Tammy? What's it like to stare into the eye of Satan's butthole?",
    "It's always a good idea to demonstrate to your coworkers that you are capable of withstanding a tremendous amount of pain.",
    "Normally, if given the choice between doing something and nothing, I’d choose to do nothing. But I will do something if it helps someone else do nothing. I’d work all night, if it meant nothing got done.",
    "On my deathbed, my final wish is to have my ex-wives rush to my side so I can use my dying breath to tell them both to go to hell one last time."
]


@register.simple_tag(name="random_swansonism")
def random_swansonism():
    return random.choice(quotes)
