
from django.db import transaction
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.contrib import admin
from django.utils.safestring import mark_safe
from django.urls import path, reverse


from gtd.admin_tools import linkify, coloured_status

from .models import ProjectCategory, Project, Task


class ProjectCategoryAdmin(admin.ModelAdmin):
    list_display = ('path', )
    ordering = ('path', )

    class Meta:
        model = ProjectCategory

admin.site.register(ProjectCategory, ProjectCategoryAdmin)


class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', )
    list_filter = ('categories', )
    search_fields = ('name', 'description')

    class Meta:
        model = Project

admin.site.register(Project, ProjectAdmin)


@transaction.atomic
def mark_as_done(modeladmin, request, queryset):
    count = queryset.update(status=Task.DONE)
    messages.success(request, "Zmieniono status {} taskom.".format(count))
    return HttpResponseRedirect(request.get_full_path())

mark_as_done.short_description = "Zakończ"


class TaskAdmin(admin.ModelAdmin):
    list_display = ('edit_id', 'name', coloured_status('status'), linkify('owner'), linkify('project'), )
    list_filter = ('status', 'owner', 'project')
    search_fields = ('name', 'project__name')
    save_on_top = True
    actions_on_bottom = True
    actions = [mark_as_done]

    def edit_id(self, obj):
        return mark_safe(
            '<a href="{}">EDYTUJ #{}</a>'.format(
                reverse('admin:tracker_task_change', args=(obj.id,)), obj.id
            )
        )
    edit_id.short_description = 'id'

    class Meta:
        model = Task

admin.site.register(Task, TaskAdmin)
