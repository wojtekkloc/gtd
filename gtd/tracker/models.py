from django.contrib.auth.models import User
from django.db import models

from simple_history.models import HistoricalRecords

from gtd.tags.models import Tag


class ProjectCategory(models.Model):
    parent = models.ForeignKey(
        'self',
        blank=True,
        null=True,
        on_delete=models.PROTECT)
    name = models.CharField(
        max_length=255
    )
    path = models.CharField(
        max_length=255,
        blank=True
    )

    added_at = models.DateTimeField(auto_now_add=True)
    edited_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural = 'Project categories'

    def __str__(self):
        if self.parent:
            return '- {}'.format(self.name)
        else:
            return self.name

    def subcategories(self):
        return ProjectCategory.objects.filter(parent=self).order_by('name')

    def save(self, *args, **kwargs):
        if self.parent:
            self.path = '{} > {}'.format(self.parent.name, self.name)
        else:
            self.path = self.name
        return super(ProjectCategory, self).save(*args, **kwargs)


class Project(models.Model):
    categories = models.ManyToManyField(
        ProjectCategory,
        blank=True,
        related_name='projects'
    )
    name = models.CharField(
        max_length=255
    )
    description = models.TextField(
        blank=True,
        null=True,
        help_text='Tutaj wpisz ogólny opis projektu - misję, założenia, ograniczenia itd itd.'
    )
    tags = models.ManyToManyField(
        Tag,
        blank=True,
    )

    added_at = models.DateTimeField(auto_now_add=True)
    edited_at = models.DateTimeField(auto_now=True)

    history = HistoricalRecords()

    class Meta:
        ordering = ('name', )

    def __str__(self):
        return self.name

    def tasks(self):
        return Task.objects.filter(projects=self)


class Task(models.Model):
    owner = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True)
    NEW = 'new'
    PENDING = 'pending'
    CANCELLED = 'cancelled'
    DONE = 'done'
    STATUS_CHOICES = (
        (NEW, 'nowe'),
        (PENDING, 'w trakcie'),
        (CANCELLED, 'anulowane'),
        (DONE, 'zrobione')
    )
    status = models.CharField(
        max_length=100,
        choices=STATUS_CHOICES,
        default=NEW
    )
    name = models.CharField(
        max_length=255
    )
    project = models.ForeignKey(
        Project, on_delete=models.CASCADE, blank=True, null=True,
    )
    link = models.URLField(
        blank=True,
        null=True
    )
    description = models.TextField(
        blank=True,
        null=True
    )
    priority = models.IntegerField(
        default=100
    )
    tags = models.ManyToManyField(
        Tag,
        blank=True,
    )

    added_at = models.DateTimeField(auto_now_add=True)
    edited_at = models.DateTimeField(auto_now=True)

    history = HistoricalRecords()

    def __str__(self):
        return self.name
